default:
  image: debian:unstable

variables:
  PYTHON: python3

### Dependency installation

.depends-debian-minimal:
  # Mandatory run-time and test-time dependencies
  before_script:
  - apt-get update
  - apt-get -y install make dpkg-dev
  - apt-get -y install python3 python3-chardet python3-setuptools python3-pytest zstd $OPTIONAL_DEPS $JOB_DEPS
  variables:
    OPTIONAL_DEPS: ""
    JOB_DEPS: ""

.depends-debian-full:
  # Additional run-time and test-time dependencies for optional features
  extends:
    .depends-debian-minimal
  variables:
    OPTIONAL_DEPS: python3-apt gpgv binutils

.depends-pip:
  before_script:
  - $PYTHON -m pip install chardet setuptools pytest $OPTIONAL_PIP_DEPS $JOB_PIP_DEPS
  variables:
    OPTIONAL_PIP_DEPS: ""
    JOB_PIP_DEPS: ""

### Unit testing runners

.unit-tests-generic:
  script:
  - LC_ALL=C $PYTHON -m pytest --doctest-modules $COVERAGE --verbose lib/
  - LC_ALL=C.UTF-8 $PYTHON -m pytest --doctest-modules $COVERAGE $COVERAGE_REPORT --verbose lib/
  variables:
    # Only generate coverage data and a report once in the test matrix
    COVERAGE: ""
    COVERAGE_REPORT: ""
    # control whether missing tools result in test failures
    FORBID_MISSING_APT_PKG: ""
    FORBID_MISSING_GPGV: ""
    FORBID_MISSING_AR: ""
    FORBID_MISSING_DPKG_DEB: ""
    FORBID_MISSING_ZSTD: ""

.unit-tests-debian:
  extends:
  - .unit-tests-generic
  script:
  - ./debian/rules lib/debian/__init__.py
  - !reference [.unit-tests-generic, script]

### Unit testing configurations (Debian)

unit-tests:
  extends:
  - .depends-debian-full
  - .unit-tests-debian
  after_script:
  - python3-coverage html
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  variables:
    # Omit --cov-report to generate a report that gitlab can pick up with its log parser
    COVERAGE: --cov --cov-branch --cov-append
    # Generate a coverage report for the pages
    COVERAGE_REPORT: --cov-report=
    JOB_DEPS: python3-pytest-cov python3-coverage
    # require optional dependencies to be installed at test time
    FORBID_MISSING_APT_PKG: "1"
    FORBID_MISSING_GPGV: "1"
    FORBID_MISSING_AR: "1"
    FORBID_MISSING_DPKG_DEB: "1"
    FORBID_MISSING_ZSTD: "1"
  artifacts:
    paths:
      - htmlcov

unit-tests-minimal:
  extends:
  - .depends-debian-minimal
  - .unit-tests-debian

unit-tests-stable:
  image: debian:stable
  extends:
  - .depends-debian-full
  - .unit-tests-debian
  variables:
    # require optional dependencies to be installed at test time
    FORBID_MISSING_APT_PKG: "1"
    FORBID_MISSING_GPGV: "1"
    FORBID_MISSING_AR: "1"

unit-tests-oldstable:
  image: debian:oldstable
  extends:
  - .depends-debian-full
  - .unit-tests-debian
  variables:
    # require optional dependencies to be installed at test time
    FORBID_MISSING_APT_PKG: "1"
    FORBID_MISSING_GPGV: "1"
    FORBID_MISSING_AR: "1"

unit-tests-oldoldstable:
  image: debian:oldoldstable
  extends:
  - .depends-debian-full
  - .unit-tests-debian
  variables:
    # require optional dependencies to be installed at test time
    FORBID_MISSING_APT_PKG: "1"
    FORBID_MISSING_GPGV: "1"
    FORBID_MISSING_AR: "1"

### Unit testing configurations (non-Debian)

unit-tests-truncated-debian:
  extends:
  - .depends-debian-minimal
  - .unit-tests-generic
  script:
  - ./debian/rules lib/debian/__init__.py
  - rm -f /usr/bin/ar /usr/bin/gpgv
  - !reference [.unit-tests-generic, script]

unit-tests-alpine:
  image: python:alpine
  extends:
  - .depends-pip
  - .unit-tests-generic

unit-tests-fedora-minimal:
  image: fedora:latest
  extends:
  - .depends-pip
  - .unit-tests-generic
  before_script:
  - dnf install -y python3 python3-pip binutils
  - !reference [.depends-pip, before_script]

unit-tests-fedora:
  image: fedora:latest
  extends:
  - .unit-tests-generic
  before_script:
  - dnf install -y python3 python3-apt python3-chardet python3-setuptools python3-pytest zstd

unit-tests-wine:
  before_script:
  - apt-get update
  - apt-get -y install python3-pip wine
  - PATH=~/.local/bin/:$PATH
  - pip install --user wenv
  - wenv init
  - rm ~/.local/share/wenv/win64/drive_c/*/python*._pth
  - wenv python -m pip install chardet setuptools pytest
  - rm -f /usr/bin/ar /usr/bin/gpgv
  script:
  - wenv python -m pytest --doctest-modules --verbose lib/
  variables:
    WENV_ARCH: win64

unit-tests-windows:
  tags:
  - shared-windows
  - windows
  - windows-1809
  rules:
  - if: $CI_SERVER_HOST !~ /debian.org$/
  extends:
  - .depends-pip
  before_script:
  - Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
  - choco install python3 --yes --force --no-progress
  - refreshenv
  - python -m pip install -U pip wheel
  - python -m pip install chardet setuptools pytest
  script:
  - python -m pytest --doctest-modules --verbose lib/
  variables:
    PYTHON: python

### Coding style checks

style:
  extends: .depends-debian-full
  script:
  - ./debian/rules qa
  - rm lib/debian/_version.py
  variables:
    JOB_DEPS: pylint mypy

### Documentation: build and deploy

docs:
  extends: .depends-debian-full
  script:
  - rm -f docs/api/*
  - ./debian/rules doc
  - mv build/sphinx/html/ docs/
  variables:
    JOB_DEPS: python3-sphinx
  artifacts:
    paths:
      - docs

pages:
  stage: deploy
  script:
  - mv docs public
  - mv htmlcov public/
  dependencies:
    - docs
    - unit-tests
  artifacts:
    paths:
    - public
  only:
  - master
